'use strict'

// same way as nodejs using require statements
var gulp = require('gulp'),
    sass = require('gulp-sass'),
    browserSync = require('browser-sync'),
    del = require('del'),
    imagemin = require('gulp-imagemin'),
    uglify = require('gulp-uglify'),
    usemin = require('gulp-usemin'),
    rev = require('gulp-rev'),
    cleanCss = require('gulp-clean-css'),
    flatmap = require('gulp-flatmap'),
    htmlmin = require('gulp-htmlmin'),
    strip = require('gulp-strip-comments');

    /**
     * gulp.src : takes files and creates a stream of objects that represent them
     * pipe : pipes the stream through another function
     * gulp.dest : destination of changed files
     * 
     * here, we create the stream and pass it through sass adding a listener
     * then deposit the resulting files in a destination
     */
gulp.task('sass', function() {
    return gulp.src('./css/*.scss')
    .pipe(sass().on('error', sass.logError))
    .pipe(gulp.dest('./css'));
});

gulp.task('sass:watch', function() {
    gulp.watch('./css/*.scss', ['sass']);
});

gulp.task('browser-sync', function() {
    var files = [
        './*.html',
        './css/*.css',
        './img/*.{png,jpg,gif}',
        './js/*.js'
    ];

    browserSync.init(files, {
        server : {
            baseDir : './'
        }
    });
});

// 'browser-sync' must be running first before 'sass:watch'
// this is the syntax
gulp.task('default', ['browser-sync'], function() {
    gulp.start('sass:watch');
});

gulp.task('clean', function() {
    return del(['dist']);
});

gulp.task('copyfonts', function() {
    gulp.src('./node_modules/font-awesome/fonts/**/*.{ttf,woff,eof,svg}*')
    .pipe(gulp.dest('./dist/fonts'));
});

gulp.task('imagemin', function() {
    return gulp.src('img/*.{png,jpg,gif}')
    .pipe(imagemin({optimizationLevel:3, progressive: true, interlaced: true}))
    .pipe(gulp.dest('dist/img'));
});

gulp.task('stripComments', function() {
    return gulp.src('./*.html')
    .pipe(strip())
    .pipe(gulp.dest('dist'));
})

gulp.task('usemin', function() {
    return gulp.src('./*.html')
    .pipe(flatmap(function(stream, file) {
        return stream.pipe(usemin({
            css : [rev()],
            html : [function() { return htmlmin({collapseWhitespace: true, ignoreCustomComments: true}) }, function() { return strip() }],
            js : [uglify(), rev()],
            inlineJs : [uglify()],
            inlineCss : [cleanCss(), 'concat']
        }))
    }))
    .pipe(gulp.dest('dist/'));
});

// 'clean' is also required first
// note that each tasks specified this way is ran in parallel
gulp.task('build', ['clean'], function() {
    gulp.start('copyfonts', 'imagemin', 'usemin');
});